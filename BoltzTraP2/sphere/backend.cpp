//    BoltzTraP2, a program for interpolating band structures and
//    calculating semi-classical transport coefficients.
//    Copyright (C) 2017-2018 Georg K. H. Madsen <georg.madsen@tuwien.ac.at>
//    Copyright (C) 2017-2018 Jesús Carrete <jesus.carrete.montana@tuwien.ac.at>
//    Copyright (C) 2017-2018 Matthieu J. Verstraete <matthieu.verstraete@ulg.ac.be>
//
//    This file is part of BoltzTraP2.
//
//    BoltzTraP2 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    BoltzTraP2 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with BoltzTraP2.  If not, see <http://www.gnu.org/licenses/>.

#include "backend.hpp"

#include <cmath>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <set>
#include <utility>

extern "C" {
#include <spglib.h>
}

Symmetry_analyzer::Symmetry_analyzer(double lattvec[3][3],
                                     double positions[][3],
                                     const int types[], int natoms,
                                     double symprec)
{
    // Obtain the rotation matrices
    this->analyze_symmetries(lattvec, positions, types, natoms,
                             symprec);
}

/// Trivial "less than" operator implementation for Eigen::Matrix3i
namespace std
{
template<> struct less<Eigen::Matrix3i>
{
    bool operator()(const Eigen::Ref<const Eigen::Matrix3i>& a,
                    const Eigen::Ref<const Eigen::Matrix3i>& b)
    {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                if (a(i, j) < b(i, j)) {
                    return true;
                }
                if (a(i, j) > b(i, j)) {
                    return false;
                }
            }
        }
        return false;
    }
};
}

void Symmetry_analyzer::analyze_symmetries(double lattvec[3][3],
                                           double positions[][3],
                                           const int types[],
                                           int natoms, double symprec)
{
    // Call spglib to get all information about symmetries
    SpglibDataset* data =
        spg_get_dataset(lattvec, positions, types, natoms, symprec);
    if (data == NULL) {
        throw Sphere_exception("spglib's spg_get_dataset"
                               " returned a NULL pointer");
    }
    // Scan through the operations and keep only a unique set of
    // rotations plus the corresponding roto-inversions.
    int nops = data->n_operations;
    std::set<Eigen::Matrix3i> unique;
    for (int i = 0; i < nops; ++i) {
        Eigen::Map<Eigen::Matrix3i> wrapper(
            &(data->rotations[i][0][0]));
        unique.insert(wrapper.transpose());
        unique.insert(-wrapper.transpose());
    }
    this->rotations.assign(unique.begin(), unique.end());
    // Free up the space allocated by spglib
    spg_free_dataset(data);
}

/// Simple class for computing squared norms of lattice vectors
class Lattice_tape
{
public:
    /// Basic constructor.
    ///
    /// @param[in] lattvec - lattice vectors, as columns
    Lattice_tape(const double lattvec[3][3])
    {
        Eigen::Map<const Eigen::Matrix3d> wrapper(&(lattvec[0][0]));
        this->metric = wrapper * wrapper.transpose();
    }
    /// Compute the squared norm of a lattice vector
    ///
    /// @param[in] v - vector in direct coordinates
    /// @return the squared norm of v
    double measure(const Eigen::Ref<const Eigen::Vector3i>& v) const
    {
        Eigen::Vector3d dv = v.cast<double>();
        return dv.transpose() * this->metric * dv;
    }

private:
    /// Metric tensor
    Eigen::Matrix3d metric;
};

/// Comparison function for pairs or tuples based on the second element
///
/// @param p1 - first operand
/// @param p2 - second operand
/// @return the result of p1.second < p2.second
template<typename T> bool compare_second(const T& p1, const T& p2)
{
    return p1.second < p2.second;
}

Sphere_equivalence_builder::Sphere_equivalence_builder(
    double lattvec[3][3], double positions[][3], const int types[],
    int natoms, double r, const int bounds[3], double symprec)
    : Equivalence_builder(lattvec, positions, types, natoms, symprec)
{
    // Generate a list of all lattice points in the intersection and
    // store their squared norms for later use
    this->create_grid(lattvec, r, bounds);
    // Initialize the point mapping
    this->mapping.resize(this->grid.size());
    std::fill(this->mapping.begin(), this->mapping.end(), -1);
    // Find an equivalence class for each point
    point_with_sqnorm lo = std::make_pair(Eigen::Vector3i::Zero(), 0.);
    point_with_sqnorm hi = std::make_pair(Eigen::Vector3i::Zero(), 0.);
    for (std::size_t i = 0; i < this->grid.size(); ++i) {
        // Each point can only be equivalent to a point with
        // the same norm. Some room is left for rounding errors
        Eigen::Vector3i point = this->grid[i].first;
        double sqnorm = this->grid[i].second;
        lo.second = (1. - symprec) * sqnorm;
        hi.second = (1. + symprec) * sqnorm;
        std::size_t lbound = std::distance(
            this->grid.begin(),
            std::lower_bound(this->grid.begin(), this->grid.end(), lo,
                             compare_second<point_with_sqnorm>));
        std::size_t ubound = std::distance(
            this->grid.begin(),
            std::upper_bound(this->grid.begin(), this->grid.end(), hi,
                             compare_second<point_with_sqnorm>));
        ubound = std::min(ubound, i);
        for (std::size_t o = 0; o < this->rotations.size(); ++o) {
            // Apply each symmetry operation to the point
            Eigen::Vector3i image = this->rotations[o] * point;
            // And compare the result to each of the candidates
            for (std::size_t j = lbound; j < ubound; ++j) {
                if (image == this->grid[j].first) {
                    // If they match, find the representative of the
                    // equivalence class, assign this point to the class
                    // and exit the loop
                    int k = j;
                    while (this->mapping[k] != k) {
                        k = this->mapping[k];
                    }
                    this->mapping[i] = k;
                    break;
                }
            }
            if (this->mapping[i] != -1) {
                break;
            }
        }
        // If no equivalence class was found for this point, start a new
        // one
        if (this->mapping[i] == -1) {
            this->mapping[i] = i;
        }
    }
}

void Sphere_equivalence_builder::create_grid(double lattvec[3][3],
                                             double r,
                                             const int bounds[3])
{
    double r2(r * r);
    Lattice_tape tape(lattvec);
    for (int i = -bounds[0]; i <= bounds[0]; ++i) {
        for (int j = -bounds[1]; j <= bounds[1]; ++j) {
            for (int k = -bounds[2]; k <= bounds[2]; ++k) {
                Eigen::Vector3i point(i, j, k);
                double n2 = tape.measure(point);
                if (n2 < r2) {
                    this->grid.push_back(std::make_pair(point, n2));
                }
            }
        }
    }
    // The list is stored in order of increasing norm
    std::sort(this->grid.begin(), this->grid.end(),
              compare_second<point_with_sqnorm>);
}

Degeneracy_counter::Degeneracy_counter(double lattvec[3][3],
                                       double positions[][3],
                                       const int types[], int natoms,
                                       double symprec)
    : Equivalence_builder(lattvec, positions, types, natoms, symprec),
      tolerance(symprec)
{
    // Obtain the rotation matrices in the reciprocal basis
    Eigen::Map<Eigen::Matrix3d> wrapper(&(lattvec[0][0]));
    Eigen::Matrix3d tmetric = wrapper.transpose() * wrapper;
    Eigen::ColPivHouseholderQR<Eigen::Matrix3d> solver;
    solver.compute(tmetric);
    for (std::size_t ir = 0; ir < this->rotations.size(); ++ir) {
        this->krotations.push_back(
            (solver.solve(
                 this->rotations[ir].cast<double>().transpose() *
                 tmetric))
                .transpose());
    }
}

int Degeneracy_counter::count_degeneracy(double point[3])
{
    // Fill the list with all possible images of the point
    this->kpoints.clear();
    Eigen::Map<Eigen::Vector3d> kpoint(point);
    for (std::size_t o = 0; o < this->krotations.size(); ++o) {
        Eigen::Vector3d image = this->krotations[o] * kpoint;
        image -= image.array().round().matrix().eval();
        this->kpoints.push_back(image);
    }
    // Initialize the point mapping
    this->mapping.resize(this->kpoints.size());
    std::fill(this->mapping.begin(), this->mapping.end(), -1);
    // Build the mapping by finding points related by translations
    for (std::size_t i = 0; i < this->kpoints.size(); ++i) {
        for (std::size_t j = 0; j < i; ++j) {
            Eigen::Vector3d delta = this->kpoints[i] - this->kpoints[j];
            delta -= delta.array().round().matrix().eval();
            if (delta.cwiseAbs().maxCoeff() < this->tolerance) {
                int k = j;
                while (this->mapping[k] != k) {
                    k = this->mapping[k];
                }
                this->mapping[i] = k;
                break;
            }
        }
        if (this->mapping[i] == -1) {
            this->mapping[i] = i;
        }
    }
    /// Count and return the number of classes in the mapping
    std::sort(this->mapping.begin(), this->mapping.end());
    return std::unique(this->mapping.begin(), this->mapping.end()) -
           this->mapping.begin();
}
